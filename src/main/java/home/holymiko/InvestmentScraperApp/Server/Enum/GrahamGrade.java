package home.holymiko.InvestmentScraperApp.Server.Enum;

public enum GrahamGrade {
    UNKNOWN, UNGRADED, DEFENSIVE, ENTERPRISING, NCAV
}
