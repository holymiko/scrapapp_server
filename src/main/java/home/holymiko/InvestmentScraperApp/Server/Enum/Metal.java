package home.holymiko.InvestmentScraperApp.Server.Enum;

public enum Metal {
    UNKNOWN, GOLD, SILVER, PLATINUM, PALLADIUM
}
