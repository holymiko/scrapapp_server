package home.holymiko.InvestmentScraperApp.Server.Enum;

public enum TickerState {
    UNKNOWN, NOTFOUND, BAD, GOOD
}
