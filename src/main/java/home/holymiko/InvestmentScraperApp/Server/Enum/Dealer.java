package home.holymiko.InvestmentScraperApp.Server.Enum;

public enum Dealer {
    UNKNOWN, BESSERGOLD, ZLATAKY, SILVERUM
}